////////////////
//// CSE 02 Pyramid Junyue Li
///

////In order to use the Scanner class
import java.util.Scanner;

public class Pyramid{
  public static void main(String[] args) {
     
     //declare an instance of the Scanner object
     Scanner myScanner = new Scanner( System.in );
    
     System.out.print("Enter the square side of the pyramid is: ");
     
    //accept user input by using the statement
     double pyramidLength = myScanner.nextDouble();
    
     System.out.print("Enter the height of the pyramid is: ");
       
     double pyramidHeight = myScanner.nextDouble();
     
     //compute the volume inside the pyramid
     double pyramidVolume = (pyramidLength * pyramidLength * pyramidHeight) / 3;
     
      //print out the final result
      System.out.println("The volume inside the pyramid is: " + pyramidVolume); 
    
  }//end of method
}//end of class
     