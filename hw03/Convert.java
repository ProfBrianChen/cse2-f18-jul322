/////////////////
//// CSE 02 Convert JunYue Li
///

//In order to use the Scanner class
import java.util.Scanner;

public class Convert{
  public static void main(String[] args) {
    
    //declare an instance of the Scanner object
    Scanner myScanner = new Scanner( System.in );
    
    System.out.print("Enter the affected area in acres: ");
    
    //accept user input by using the statement
    double acresArea = myScanner.nextDouble();
    
    System.out.print("Enter the rainfall in the affected area: " );
    
    double rainFall = myScanner.nextDouble();
    
    //compute affected acres times inches of rain and generate gallons
    
    double gallonsRain = acresArea * rainFall * 27154;
    
    //convert gallons to cubic miles
    
    double cublicRain = gallonsRain * 9.08169 * Math.pow(10,-13);
    
    //print the final result
    System.out.println(cublicRain + "cubic miles");
    
     }//end of mathod
  }//end of class