///////////
//// CSE 02 Junyue Li  HW05
/////

import java.lang.Math;
import java.util.Scanner;
public class Hw05{
  public static void main(String[] args) {
    
    Scanner myScanner = new Scanner( System.in );
   
    
    System.out.println("please enter how many times you want to generate hands: ");
   while ( myScanner.hasNextInt() == false){
    System.out.println("wrong, please enter an integer");
    myScanner.next(); 
   }
    
    int handNum = myScanner.nextInt();
    
    System.out.println(" ");
    
    // declare
    int cardVal1 = 0;
    int cardVal2 = 0;
    int cardVal3 = 0;
    int cardVal4 = 0;
    int cardVal5 = 0;
    
    int faceVal1 = 0;
    int faceVal2 = 0;
    int faceVal3 = 0;
    int faceVal4 = 0;
    int faceVal5 = 0;
    
    int onePair = 0;
    int twoPair = 0;
    int threeKind = 0;
    int fourKind = 0;
    String kind = " ";
    int equal = 0;
    
    int check = 0;
    
    while ( check < handNum ){
           cardVal1 = (int)(Math.random()*52)+1; 
           cardVal2 = (int)(Math.random()*52)+1; 
           cardVal3 = (int)(Math.random()*52)+1; 
           cardVal4 = (int)(Math.random()*52)+1; 
           cardVal5 = (int)(Math.random()*52)+1; 
       
          while ( cardVal1 == cardVal2 || cardVal1 == cardVal3 || cardVal1 == cardVal4 || cardVal1 == cardVal5 ) {
          cardVal1 = (int)(Math.random()*52)+1; 
          }
          while ( cardVal2 == cardVal1 || cardVal2 == cardVal3 || cardVal2 == cardVal4 || cardVal2 == cardVal5 ) {
          cardVal2 = (int)(Math.random()*52)+1; 
          }
          while ( cardVal3 == cardVal1 || cardVal3 == cardVal2 || cardVal3 == cardVal4 || cardVal3 == cardVal5 ) {
          cardVal3 = (int)(Math.random()*52)+1; 
          }
          while ( cardVal4 == cardVal1 || cardVal4 == cardVal2 || cardVal4 == cardVal3 || cardVal4 == cardVal5 ) {
          cardVal4 = (int)(Math.random()*52)+1; 
          }
          while ( cardVal5 == cardVal1 || cardVal5 == cardVal2 || cardVal5 == cardVal3 || cardVal5 == cardVal4 ) {
          cardVal5 = (int)(Math.random()*52)+1; 
          }
          
          faceVal1 = cardVal1 % 13;
          faceVal2 = cardVal2 % 13;
          faceVal3 = cardVal3 % 13;
          faceVal4 = cardVal4 % 13;
          faceVal5 = cardVal5 % 13;
                 
          
            
          if ( faceVal1 == faceVal2 )
            equal++;
           if ( faceVal1 == faceVal3 )
             equal++;
             if ( faceVal1 == faceVal4 )
               equal++;
               if ( faceVal1 == faceVal5 )
                 equal++;
          if ( faceVal2 == faceVal3 )
              equal++;
           if ( faceVal2 == faceVal4 )
                equal++;
             if ( faceVal2 == faceVal5 )
                 equal++;
          if ( faceVal3 == faceVal4 )
            equal++;
           if ( faceVal3 == faceVal5 )
             equal++;
          if ( faceVal4 == faceVal5 )
            equal++;
                 
          if ( equal == 6 ){
               kind = "Four-of-a-kind";
            fourKind++;
          }
           else if ( equal == 3 ){
                 kind = "Three-of-a-kind";
             threeKind++;
           }
             else if ( equal == 2){
                   kind = "Two-pair";
               twoPair++;
             }
              else if ( equal == 1 ){
                    kind = "One-pair";
                 onePair++;
              }
                else if ( equal == 0 ){
                  kind = "Different";
              }
              
      check++;      
       }
    
   double fourProb = fourKind / (double)handNum;
   double threeProb = threeKind / (double)handNum;
   double twoProb = twoPair / (double)handNum;
   double oneProb = onePair / (double)handNum;
   
   System.out.println("The number of loops: " + handNum ); 
   System.out.printf("probability of Four-of-a-kind: %.3f\n", fourProb);
   System.out.printf("probability of Three-of-a-kind: %.3f\n", threeProb);
   System.out.printf("probability of Two-pairs: %.3f\n", twoProb);
   System.out.printf("probability of One-pair: %.3f\n", oneProb);
    
    
  }
}
    